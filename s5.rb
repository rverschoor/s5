#!/usr/bin/env ruby

require 'faraday'
require 'json'
require 'yaml'

class S5

  def initialize
    name = arguments
    config_read name
    secrets
    api
    status
    # presence
  end

  def arguments
    if ARGV.length == 1
      name = ARGV[0]
    else
      # TODO accept ad-hoc values from commandline
      puts 'Supply a config entry name, eg "$ ./s5.rb dogwalk"'
      exit
    end
    name
  end

  def config_read(name)
    config = YAML.load(File.read('config.yaml'))
    entry = config.find { |entry| entry['name'] == name }
    unless entry
      puts "Config entry #{name} not found"
      exit
    end
    @emoji = entry['emoji']
    @text = entry['text']
    @time = entry['time']
    @away = entry['away']
  end

  def api
    if not @token or not @cookie
      puts "No secrets"
      exit
    end
    @conn = Faraday.new(
      url: 'https://slack.com/api',
      headers: {'Cookie' => "d=#{@cookie}", 'Content-Type' => 'application/json; charset=utf-8'}
    ) do |faraday|
      faraday.request :url_encoded
      faraday.request :authorization, "Bearer", @token
      faraday.response :json
    end
  end

  def secrets
    `op signin`
    @host = `op read "op://Private/stom/secrets/host"`.strip
    @cookie = `op read "op://Private/stom/secrets/cookie"`.strip
    @token = `op read "op://Private/stom/secrets/token"`.strip
  end

  def status
    expiration = calculate_time
    data = { profile: { status_emoji: @emoji, status_text: @text, status_expiration: expiration } }
    resp = @conn.post('users.profile.set', data.to_json)
    api_resp resp
  end

  def calculate_time
    # TODO
    0
  end

  def presence
    # TODO
    # data = { presence: 'away' }  # away/auto
    # resp = @conn.post('users.setPresence', data.to_json)
    # api_resp resp
  end

  def api_resp(resp)
    unless resp.status == 200 and resp.body['ok'] == true
      puts resp.status
      puts resp.body
      exit
    end
  end

end

S5.new

